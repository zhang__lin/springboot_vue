const mix = require('laravel-mix');
const webpack = require("webpack")

mix.setPublicPath('public/')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    module: {
        rules: [
          {
            test: /\.php?$/,
            use: [
              {loader: 'ignore-loader',}
            ]
          },
        ]
    }
})

/**
 * 后台静态 main JS
 */
mix.js('resources/js/app.js', 'public/js');