const getters = {
  device: state => state.app.device,
  submenus: state => state.app.submenus,
  avatar: state => state.user.avatar,
  user: state => state.user.user,
  menus: state => state.user.menus,
  login_admin_permissions : state => state.user.login_admin_permissions,
  is_super : state => state.user.is_super,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
}
export default getters
