import {getLoginUser, removeLoginUser, setLoginUser} from '../../utils/auth'

const loginUser = getLoginUser();
const menus = sessionStorage.getItem('admin_menus');
const login_admin_permissions = sessionStorage.getItem('admin_permissions');

const user = {
    state: {
        user: loginUser ? loginUser : null,
        menus: menus ? JSON.parse(menus) : [],
        login_admin_permissions: login_admin_permissions ? JSON.parse(login_admin_permissions) : [],
        is_super: parseInt(sessionStorage.getItem('admin_is_super')),
    },

    mutations: {
        SET_USER: (state, user) => {
            state.user = user
        },
        SET_AUTHORIZATION: (state, Authorization) => {
            if (Authorization == null) {
                sessionStorage.removeItem('Authorization');
            } else {
                sessionStorage.setItem('Authorization', Authorization);
            }
            window.axios.defaults.headers.common['Authorization'] = Authorization;
            state.Authorization = Authorization
        },
        SET_MENUS: (state, menus) => {
            if (menus == null) {
                sessionStorage.removeItem('admin_menus');
            } else {
                sessionStorage.setItem('admin_menus', JSON.stringify(menus));
            }
            state.menus = menus
        },
        SET_PERMISSIONS: (state, login_admin_permissions) => {
            if (login_admin_permissions == null) {
                sessionStorage.removeItem('admin_permissions');
            } else {
                sessionStorage.setItem('admin_permissions', JSON.stringify(login_admin_permissions));
            }
            state.login_admin_permissions = login_admin_permissions
        },
        SET_SUPER: (state, is_super) => {
            state.is_super = is_super
            if (is_super == null) {
                sessionStorage.removeItem('admin_is_super');
            } else {
                sessionStorage.setItem('admin_is_super', is_super);
            }
        }
    },

    actions: {
        //更新登录用户
        Login({commit}) {
            return new Promise((resolve, reject) => {
                axios.get(basePath + '/loginUser').then(function (res) {
                    setLoginUser(res.data);
                    commit('SET_USER', res.data)
                });
                resolve();
            })
        },
        Authorization({commit}, Authorization) {
            commit('SET_AUTHORIZATION', Authorization)
        },
        // 退出登录
        LogOut({commit}) {
            return new Promise(resolve => {
                removeLoginUser()
                commit('SET_USER', null)
                commit('SET_MENUS', null)
                commit('SET_PERMISSIONS', null)
                commit('SET_SUPER', null)
                commit('SET_AUTHORIZATION', null)
                commit('SET_SUBMENUS', null)
                window.location.reload();
                resolve()
            })
        },

        //更新用户菜单权限
        FreshRole({commit}) {
            return new Promise(resolve => {
                axios.post(basePath + '/freshRole').then(function (res) {
                    if (res.success) {
                        commit('SET_MENUS', res.data.menus);
                        commit('SET_PERMISSIONS', res.data.operate_permission);
                        commit('SET_SUPER', res.data.is_super ? 1 : 0);
                    }
                });
                resolve()
            })
        },
    }
}

export default user
