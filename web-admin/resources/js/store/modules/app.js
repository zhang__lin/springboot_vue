const submenus = sessionStorage.getItem('submenus');
const app = {
    state: {
        device: 'desktop',
        submenus: submenus ? JSON.parse(submenus) : [],
    },
    mutations: {
        TOGGLE_DEVICE: (state, device) => {
            state.device = device
        },
        SET_SUBMENUS: (state, submenus) => {
            if (submenus == null) {
                sessionStorage.removeItem('submenus');
            } else {
                sessionStorage.setItem('submenus', JSON.stringify(submenus));
            }
            state.submenus = submenus
        }
    },
    actions: {
        ToggleDevice({commit}, device) {
            commit('TOGGLE_DEVICE', device)
        },
        SetSubMenu({commit}, submenus) {
            commit('SET_SUBMENUS', submenus)
        }
    }
}

export default app
