/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import './config';
require('./bootstrap');
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../scss/app.scss'
import App from './App.vue'
import router from './router'
import store from './store'
import './permission'
import './icon'
import IconSvg from '../layout/components/icon-svg'
import * as filters from './filters' // global filters

Vue.use(ElementUI)
Vue.component('icon-svg', IconSvg)

// register global utility filters
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.vueApp = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})

window.basePath = 'http://admin.xfw.com:8080/api';
window.webPath = 'https://www.pmmaster.com.cn/api/';
