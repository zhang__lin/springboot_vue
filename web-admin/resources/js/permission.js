import router from './router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getLoginUser } from './utils/auth'

NProgress.configure({ showSpinner: false })

const whiteList = ['/login']

const loginUser = getLoginUser();

router.beforeEach((to, from, next) => {
    NProgress.start()
    if (loginUser) {
      if (to.path === '/login') {
        next({ path: to.query.redirect || '/' })
        NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
      } else {
        next()
      }
    } else {
      if (whiteList.indexOf(to.path) !== -1) {
        next()
      } else {
        // next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
        next(`/login`) // 否则全部重定向到登录页
        NProgress.done()
      }
    }
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
