window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.Popper = require('popper.js').default;

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.Vue = require('vue');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Authorization'] = sessionStorage.getItem("Authorization");

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response.data;
}, function (error) {
    if (error.response.status == '401') {
        vueApp.$store.dispatch('LogOut');
        var cb = vueApp.$route.fullPath;
        window.location.href = '/#/login?redirect=' + cb;
    } else if (error.response.status == '403') {
        vueApp.$message.error("没有访问权限");
        // vueApp.$router.replace('/403');
    } else if (error.response.status == '404') {
        vueApp.$message.error(error.response.data.msg);
    } else if (error.response.status == '500') {
        vueApp.$message.error("系统异常：" + error.response.data.message);
    }
    return false;
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.io = require('socket.io-client');

// window.Echo = new Echo({
//   broadcaster: 'socket.io',
//   host: window.location.hostname + ':6001'
// });
