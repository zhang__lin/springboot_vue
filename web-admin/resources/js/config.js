window.web_config = {
    copyright:'2019-2021',
    name:'四川蜀联弘兴科技有限公司',
    en_name: 'Sichuan Shulian Hongxing Technology Co., Ltd.',
    webPath:'baobangong.com',
    beian:'蜀ICP备00000000号-0',
    beino:'00000000000000',
    gongan:'蜀公网安备00000000000000号',
    tell:'028-88888888',
    fullPath:'https://www.baobangong.com',
};