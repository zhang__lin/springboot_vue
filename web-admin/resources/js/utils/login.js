import $ from 'jquery'
const login = {
  width: null,
  height: null,
  largeHeader: null,
  canvas: null,
  ctx: null,
  circles: null,
  target: null,
  animateHeader: true,
  Circle: function() {
    var _this = this
    // constructor
    _this.pos = {}
    initCircle()
    function initCircle() {
      _this.pos.x = Math.random() * login.width
      _this.pos.y = login.height + Math.random() * 100
      _this.alpha = 0.1 + Math.random() * 0.3
      _this.scale = 0.1 + Math.random() * 0.3
      _this.velocity = Math.random()
    }
    this.draw = function() {
      if (_this.alpha <= 0) {
        initCircle()
      }
      _this.pos.y -= _this.velocity
      _this.alpha -= 0.0005
      login.ctx.beginPath()
      login.ctx.arc(_this.pos.x, _this.pos.y, _this.scale * 10, 0, 2 * Math.PI, false)
      login.ctx.fillStyle = 'rgba(255,255,255,' + _this.alpha + ')'
      login.ctx.fill()
    }
  },
  initHeader: function() {
    $('#app').append('<canvas id="ani-canvas"></canvas>')

    this.width = window.innerWidth
    this.height = window.innerHeight
    this.target = {
      x: 0,
      y: this.height
    }
    this.largeHeader = document.getElementById('app')
    this.largeHeader.style.height = this.height + 'px'
    this.canvas = document.getElementById('ani-canvas')
    this.canvas.width = this.width
    this.canvas.height = this.height
    this.ctx = this.canvas.getContext('2d')
    this.circles = []
    for (var x = 0; x < this.width * 0.5; x++) {
      var c = new this.Circle()
      this.circles.push(c)
    }
    this.animate()
  },
  animate: function() {
    if (login.animateHeader) {
      login.ctx.clearRect(0, 0, login.width, login.height)
      for (var i in login.circles) {
        login.circles[i].draw()
      }
    }
    requestAnimationFrame(login.animate)
  },
  addListeners: function() {
    window.addEventListener('scroll', this.scrollCheck)
  },
  scrollCheck: function() {
    if (document.body.scrollTop > this.height) {
      this.animateHeader = false
    } else {
      this.animateHeader = true
    }
  }
}
export default login
