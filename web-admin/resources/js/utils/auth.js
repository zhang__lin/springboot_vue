const TokenKey = 'loginUser';

export function getLoginUser() {
    return sessionStorage.getItem(TokenKey) ? JSON.parse(sessionStorage.getItem(TokenKey)) : null;
}

export function setLoginUser(token) {
    return sessionStorage.setItem(TokenKey, JSON.stringify(token))
}

export function isLogin() {
    const loginUser = sessionStorage.getItem(TokenKey);
    return !!loginUser;
}

export function removeLoginUser() {
    return sessionStorage.removeItem(TokenKey)
}
