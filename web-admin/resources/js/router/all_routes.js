const super_routes = [
    {
        name: '系统设置', value: 'system', icon: 'icon_systems',
        children: [
            {name: '菜单管理', value: 'system/menu', component: 'system/menu', is_hidden: 0},
            {name: '权限管理', value: 'system/permission', component: 'system/permission', is_hidden: 0},
            {name: '角色管理', value: 'system/role', component: 'system/role', is_hidden: 0},
            {name: '管理员管理', value: 'system/admin', component: 'system/admin', is_hidden: 0},
        ]
    },
    {
        name: '机构管理', value: 'company', icon: 'icon_supplier',
        children: [
            {name: '机构列表', value: 'company/index', component: 'company/index', is_hidden: 0},
            {name: '机构添加', value: 'company/add', component: 'company/add', is_hidden: 0, noCache: 1,},
            {name: '部门管理', value: 'company/department', component: 'company/department', is_hidden: 1},
            {name: '机构管理员管理', value: 'company/admins', component: 'company/admins', is_hidden: 1, noCache: 1},
        ]
    },
    {
        name: '日志管理', value: 'logs', icon: 'icon_logs',
        children: [
            {name: '管理员日志', value: 'system/log', component: 'system/log', is_hidden: 0},
        ]
    },
];

export default super_routes;
