import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '../../layout/Layout'
import super_routes from './all_routes'

Vue.use(Router)

/**
 * 公共路由
 */
export const publicRouterMap = [
    {
        id: 'login',
        path: '/login',
        component: () => import ('../../views/common/login.vue'),
        hidden: true
    },
    {
        id: 'home',
        path: '/',
        name: '首页',
        component: Layout,
        meta: {icon: 'icon_index'},
        redirect: '/index',
    },
    {
        id: 'empty',
        path: '/',
        component: Layout,
        hidden: true,
        children: [
            {
                path: '/index',
                name: '主页',
                component: () => import ('../../views/index/index.vue'),
                meta: {icon: 'icon_index', title: '首页'}
            },
            {
                path: '/setting',
                name: '账户设置',
                component: () => import ('../../views/common/setting.vue'),
                meta: {icon: 'icon_index', title: '账户设置'}
            },
            {
                path: '/403',
                name: '403',
                component: () => import ('../../views/common/403.vue'),
                hidden: true,
            },
            {
                path: '/404',
                name: '404',
                component: () => import ('../../views/common/404.vue'),
                hidden: true,
            },
        ]
    },
    {
        id: '*',
        path: '*',
        redirect: '/404',
        hidden: true
    }
];

//权限菜单处理
let permission_routes = [];
let menus = null;
if (parseInt(sessionStorage.getItem('admin_is_super'))) {
    menus = super_routes;
} else {
    let tmp = JSON.parse(sessionStorage.getItem('admin_menus'));
    menus = tmp ? tmp.children : null;
}
if (menus) {
    for (let i = 0; i < menus.length; i++) {
        let parent = menus[i];
        let menu = {
            id: parent.value,
            path: '/' + parent.value,
            component: Layout,
            name: parent.name,
            meta: {
                title: parent.name,
                icon: parent.icon
            },
            children: []
        };
        for (let m = 0; m < parent.children.length; m++) {
            let child = parent.children[m];
            menu.children.push(
                {
                    path: '/' + child.value,
                    name: child.name,
                    component: () => import ('../../views/' + child.component),
                    meta: {
                        title: child.name,
                        noCache: child.noCache ? true : false,
                    },
                    hidden: child.isHidden,
                },
            );
        }
        permission_routes.push(menu);
    }
}
const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({y: 0}),
    routes: publicRouterMap.concat(permission_routes)
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router
