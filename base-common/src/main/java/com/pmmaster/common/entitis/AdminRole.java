package com.pmmaster.common.entitis;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;

/**
 * xfw_admin_roles
 *
 * @author Always_
 * @date 2020-02-05 18:06:24
 */
@Data
public class AdminRole extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 角色名称
     */
    @NotNull(message = "角色名称不能为空")
    private String name;

    /**
     * 状态0删除1正常
     */
    private Integer state;

    /**
     * 备注
     */
    private String description;

    /**
     * 关联权限
     */
    private ArrayList<Integer> permissions;
    /**
     * 关联菜单
     */
    private ArrayList<Integer> menus;

    public AdminRole() {

    }

    public AdminRole(Integer id, Integer state, Date updatedAt) {
        this.setId(id);
        this.setState(state);
        this.setUpdatedAt(updatedAt);
    }
}
