package com.pmmaster.common.entitis;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色菜单
 * xfw_admin_role_menus
 *
 * @author Always_
 * @date 2020-02-05 18:06:24
 */
@Data
public class AdminRoleMenu implements Serializable {
    /**
     *
     */
    private Integer roleId;

    /**
     *
     */
    private Integer menuId;

    private static final long serialVersionUID = 1L;

    public AdminRoleMenu() {
    }

    public AdminRoleMenu(Integer roleId, Integer menuId) {
        this.setRoleId(roleId);
        this.setMenuId(menuId);
    }
}
