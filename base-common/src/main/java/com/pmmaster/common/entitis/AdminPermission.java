package com.pmmaster.common.entitis;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;

/**
 * xfw_admin_permissions
 *
 * @author Always_
 * @date 2020-02-05 18:06:24
 */
@Data
public class AdminPermission extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 权限名称
     */
    private String name;

    /**
     *
     */
    private String alias;

    /**
     * 权限地址
     */
    private String value;

    /**
     * 父权限@text
     */
    private Integer parentId;

    /**
     *
     */
    private Date deletedAt;

    private ArrayList<AdminPermission> children;
}
