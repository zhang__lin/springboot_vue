package com.pmmaster.common.entitis;

import lombok.Data;

import java.util.Date;

/**
 * 图片库
 * xfw_files
 *
 * @author Always_
 * @date 2020-02-10 18:11:28
 */
@Data
public class FileEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 上传位置
     */
    private String type;

    /**
     * 上传的文件名
     */
    private String originalname;

    /**
     * 文件类型
     */
    private String mimetype;

    /**
     * 所在路径
     */
    private String filePath;

    /**
     * 文件大小
     */
    private Double size;

    /**
     * 图片尺寸
     */
    private String widthHeight;

    public FileEntity() {
    }

    public FileEntity(String type, String originalFilename, String realType, String filePath, long size) {
        this.setType(type);
        this.setOriginalname(originalFilename);
        this.setMimetype(realType);
        this.setFilePath(filePath);
        this.setSize((double) size);
        this.setCreatedAt(new Date());
    }
}
