package com.pmmaster.common.entitis;

import com.pmmaster.common.validated.Phone;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class Admin extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 所在部门
     */
    @NotNull(message = "所在部门不能为空")
    private Integer departmentId;

    private Integer companyId;

    @Builder.Default
    private Integer[] roleList = {};

    @Builder.Default
    private String role = "";

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 机构名称
     */
    private String companyName;

    @NotNull(message = "账号不能为空")
    private String name;

    private String nickName;

    @Phone
    private String phone;

    @NotNull(message = "密码不为空")
    private String password;

    private String realName;

    private String idNumber;

    @Email(message = "邮箱格式有误")
    private String email;

    private String openId;

    private String avatar;

    private Integer state;

    private Date lastLoginAt;

    private String lastLoginIp;

    private Date nowLoginAt;

    private String nowLoginIp;

    private String code;

    private String token;

    private String keyword;
}

