package com.pmmaster.common.entitis;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 机构
 * xfw_companies
 *
 * @author Always_
 * @date 2020-02-10 14:23:09
 */
@Data
public class Company extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 公司角色CompanyRole Enum
     */
    private Integer role;

    /**
     * 机构名称
     */
    private String name;

    /**
     * 机构简称
     */
    private String shortName;

    /**
     * 客户等级1-5级
     */
    private BigDecimal rate;

    /**
     * 机构类别
     */
    private Integer companyType;

    /**
     * 状态：1正常0禁用
     */
    private Integer state;

    /**
     * 开票名称
     */
    private String ticketName;

    /**
     * 税号
     */
    private String registerNo;

    /**
     * 注册场所地址
     */
    private String registerAddress;

    /**
     * 注册电话
     */
    private String registerPhone;

    /**
     * 开户行名称
     */
    private String bankName;

    /**
     * 开户行账号
     */
    private String bankNo;

    /**
     * 所在省市县
     */
    private String regions;

    private String[] regionsArr;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 固定电话
     */
    private String mobile;

    /**
     * 官网地址
     */
    private String url;

    /**
     * 供应商类型
     */
    private Integer supplierType;

    /**
     * 联系人姓名
     */
    private String contactName;

    /**
     * 联系人手机号
     */
    private String contactPhone;

    /**
     * 营业执照
     */
    private String licencePic;

    /**
     * 机构备注
     */
    private String desc;

    /**
     * 成立时间
     */
    private Date buildAt;

    private String urlPrepend;
}
