package com.pmmaster.common.entitis;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;

/**
 * 菜单
 * xfw_admin_menus
 *
 * @author Always_
 * @date 2020-02-04 19:14:30
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AdminMenu extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单名称
     */
    @NotNull(message = "菜单名不能为空")
    private String name;

    /**
     * 链接地址
     */
    private String value;

    /**
     * 模版地址
     */
    private String component;

    /**
     * icon
     */
    private String icon;

    /**
     * 是否隐藏1是0否
     */
    private Integer isHidden;

    /**
     *
     */
    @NotNull(message = "父级不能为空")
    private Integer parentId;

    /**
     * 排序
     */
    private Integer order;

    /**
     *
     */
    private Date deletedAt;

    private ArrayList<AdminMenu> children;
}
