package com.pmmaster.common.entitis;

import lombok.Data;

import java.io.Serializable;

/**
 * xfw_admin_role_permissions
 *
 * @author Always_
 * @date 2020-02-05 18:06:24
 */
@Data
public class AdminRolePermission implements Serializable {
    /**
     *
     */
    private Integer permissionId;

    /**
     *
     */
    private Integer roleId;

    private static final long serialVersionUID = 1L;

    public AdminRolePermission() {
    }

    public AdminRolePermission(Integer roleId, Integer permissionId) {
        this.setPermissionId(permissionId);
        this.setRoleId(roleId);
    }
}
