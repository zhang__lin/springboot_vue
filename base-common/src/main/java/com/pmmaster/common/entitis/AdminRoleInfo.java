package com.pmmaster.common.entitis;

import lombok.Data;

import java.io.Serializable;

/**
 * xfw_admin_role_infos
 *
 * @author Always_
 * @date 2020-02-05 18:06:24
 */
@Data
public class AdminRoleInfo implements Serializable {
    /**
     *
     */
    private Integer userId;

    /**
     * 角色编号
     */
    private Integer roleId;

    /**
     * 角色名称
     */
    private String roleName;

    private static final long serialVersionUID = 1L;

    public AdminRoleInfo() {
    }

    public AdminRoleInfo(Integer admin_id, Integer role_id) {
        this.setRoleId(role_id);
        this.setUserId(admin_id);
    }
}
