package com.pmmaster.common.entitis;

import lombok.Data;

/**
 * 机构开票信息
 * xfw_company_invoices
 *
 * @author Always_
 * @date 2020-02-10 14:23:09
 */
@Data
public class CompanyInvoice extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 公司ID
     */
    private Integer companyId;

    /**
     * 发票抬头
     */
    private String headerTitle;

    /**
     * 发票类型0增值税普通发票1企业增值税普通发票2增值税专用发票3组织（非企业）增值税普通发票
     */
    private Byte type;

    /**
     * 税务登记证号
     */
    private String registerNo;

    /**
     * 开户行名称
     */
    private String bankName;

    /**
     * 基本开户账号
     */
    private String bankNo;

    /**
     * 注册场所地址
     */
    private String registerAddress;

    /**
     * 注册固定电话
     */
    private String registerPhone;

}
