package com.pmmaster.common.entitis;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 机构部门关联
 * xfw_company_departments
 *
 * @author Always_
 * @date 2020-02-10 14:23:09
 */
@Data
public class CompanyDepartment extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 机构ID
     */
    private Integer companyId;

    /**
     * 部门名称
     */
    @NotNull(message = "部门名称不能为空")
    private String name;

    /**
     * 机构名称
     */
    private String companyName;

    /**
     * 状态1正常0禁用
     */
    private Integer state;

}
