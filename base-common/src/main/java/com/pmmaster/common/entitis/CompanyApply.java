package com.pmmaster.common.entitis;

import lombok.Data;

/**
 * 机构账户升级申请
 * xfw_company_applies
 *
 * @author Always_
 * @date 2020-02-10 14:23:09
 */
@Data
public class CompanyApply extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 申请用户ID
     */
    private Integer userId;

    /**
     * 机构名称
     */
    private String name;

    /**
     * 简称
     */
    private String shortName;

    /**
     * 所在部门
     */
    private String departmentName;

    /**
     * 机构类型
     */
    private Integer companyType;

    /**
     * 所在地
     */
    private String regions;

    /**
     * 详细地
     */
    private String address;

    /**
     * 发票类型
     */
    private String type;

    /**
     * 发票抬头
     */
    private String headerTitle;

    /**
     * 税号
     */
    private String registerNo;

    /**
     * 开户行名称
     */
    private String bankName;

    /**
     * 开户行账户
     */
    private String bankNo;

    /**
     * 营业执照
     */
    private String licencePic;

    /**
     * 状态0待审核1审核通过2审核驳回
     */
    private Byte state;

    /**
     * b备注
     */
    private String desc;

}
