package com.pmmaster.common.entitis;

import java.io.Serializable;
import java.util.Date;

public class BaseEntity implements Serializable {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 当前页数
     */
    private Integer page = 1;

    /**
     * 每页显示数
     */
    private Integer pageSize = 10;

    /**
     * 版本号
     */
    private Date updatedAt;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 开始时间
     */
    private Date startAt;

    /**
     * 结束时间
     */
    private Date EndAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return EndAt;
    }

    public void setEndAt(Date endAt) {
        EndAt = endAt;
    }
}
