package com.pmmaster.common.validated;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {})
@Retention(RetentionPolicy.RUNTIME)
@Pattern(regexp = "1[3456789]\\d{9}", message = "手机号格式有误")
@NotNull(message = "手机号不能为空")
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD})
public @interface Phone {
    String message() default "手机号格式有误！";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
