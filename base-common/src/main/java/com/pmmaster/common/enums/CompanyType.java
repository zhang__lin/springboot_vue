package com.pmmaster.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 机构性质
 *
 * @Author: Always_
 * @Date: 2020/2/10
 */
public enum CompanyType {
    STATE_ENTERPRISE(1, "国有企业"),
    GOVERNMENT(2, "政府机关"),
    FOREIGN_ENTERPRISE(3, "外资企业"),
    JOIN_ENTERPRISE(4, "合资企业"),
    PRIVATE_ENTERPRISE(5, "私营企业"),
    CHANNEL(6, "渠道商"),
    FRANCHISEES(7, "加盟商"),
    EDU(8, "教育机构"),
    OTHER(9, "其他");

    private Integer code;
    private String desc;

    CompanyType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> list() {
        Map<Integer, String> map = new HashMap<>();
        for (CompanyType c : CompanyType.values()) {
            map.put(c.code, c.desc);
        }
        return map;
    }

    //通过索引获取描述
    public static String getDesc(Integer code) {
        for (CompanyType c : CompanyType.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    //通过描述获取索引
    public static Integer getCode(String desc) {
        if (desc == null) desc = "";
        for (CompanyType c : CompanyType.values()) {
            if (desc.equals(c.desc)) {
                return c.code;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
