package com.pmmaster.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 机构角色类型
 *
 * @Author: Always_
 * @Date: 2020/2/10
 */
public enum CompanyRole {
    CUSTOMER(1, "客户机构"),
    PARTNER(2, "合作伙伴"),
    SUPPLIER(3, "供应商"),
    SELF(99, "平台");

    private Integer code;
    private String name;

    CompanyRole(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static Map<Integer, String> list() {
        Map<Integer, String> map = new HashMap<>();
        for (CompanyRole c : CompanyRole.values()) {
            map.put(c.code, c.name);
        }
        return map;
    }

    //通过索引获取描述
    public static String getName(Integer code) {
        for (CompanyRole c : CompanyRole.values()) {
            if (c.getCode().equals(code)) {
                return c.name;
            }
        }
        return null;
    }

    //通过描述获取索引
    public static Integer getCode(String name) {
        if (name == null) name = "";
        for (CompanyRole c : CompanyRole.values()) {
            if (name.equals(c.name)) {
                return c.code;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
