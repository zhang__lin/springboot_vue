package com.pmmaster.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 供应商类型
 *
 * @Author: Always_
 * @Date: 2020/2/10
 */
public enum SupplierType {
    FIRM(1, "厂商"),
    GENERAL_AGENCY(2, "总代理"),
    AREA_AGENCY(3, "区域代理"),
    PROVINCE_AGENCY(4, "省代理"),
    CITY_AGENCY(5, "市代理"),
    DEALER(6, "经销商");

    private Integer code;
    private String desc;

    SupplierType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> list() {
        Map<Integer, String> map = new HashMap<>();
        for (SupplierType c : SupplierType.values()) {
            map.put(c.code, c.desc);
        }
        return map;
    }

    //通过索引获取描述
    public static String getDesc(Integer code) {
        for (SupplierType c : SupplierType.values()) {
            if (c.getCode().equals(code)) {
                return c.desc;
            }
        }
        return null;
    }

    //通过描述获取索引
    public static Integer getCode(String desc) {
        if (desc == null) desc = "";
        for (SupplierType c : SupplierType.values()) {
            if (desc.equals(c.desc)) {
                return c.code;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
