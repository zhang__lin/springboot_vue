package com.pmmaster.common.enums;

/**
 * @Author: Always_
 * @Date: 2020/2/8
 */
public enum CommonState {
    NORMAL(1, "正常"),
    DELETED(0, "禁用");

    private Integer state;
    private String desc;

    CommonState(Integer state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    //通过索引获取描述
    public static String getDesc(Integer state) {
        for (CommonState c : CommonState.values()) {
            if (c.getState().equals(state)) {
                return c.desc;
            }
        }
        return null;
    }

    //通过描述获取索引
    public static Integer getState(String desc) {
        if (desc == null) desc = "";
        for (CommonState c : CommonState.values()) {
            if (desc.equals(c.desc)) {
                return c.state;
            }
        }
        return null;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
