package com.pmmaster.common.api;


import com.pmmaster.common.enums.ResultCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Result<T> implements Serializable {
    private long code;
    private long status;
    private String msg;
    private boolean success;
    private T data;

    protected Result() {
    }

    protected Result(long code, String msg, T data) {
        this.code = code;
        this.status = code;
        this.success = code == ResultCode.SUCCESS.getCode();
        this.msg = msg;
        this.data = data;
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 添加结果
     *
     * @param i
     * @param <T>
     * @return
     */
    public static <T> Result<T> addResult(int i) {
        return new Result<T>(i > 0 ? ResultCode.SUCCESS.getCode() : ResultCode.FAILED.getCode(), i > 0 ? "添加成功" : "添加失败",
            null);
    }

    /**
     * 更新结果
     *
     * @param i
     * @param <T>
     * @return
     */
    public static <T> Result<T> updateResult(int i) {
        return new Result<T>(i > 0 ? ResultCode.SUCCESS.getCode() : ResultCode.FAILED.getCode(), i > 0 ? "更新成功" : "更新失败",
            null);
    }

    /**
     * 操作结果
     *
     * @param i
     * @param <T>
     * @return
     */
    public static <T> Result<T> optionResult(int i) {
        return new Result<T>(i > 0 ? ResultCode.SUCCESS.getCode() : ResultCode.FAILED.getCode(), i > 0 ? "操作成功" : "操作失败",
            null);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static <T> Result<T> success(T data, String message) {
        return new Result<T>(ResultCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param errorCode 错误码
     */
    public static <T> Result<T> failed(ResultCode errorCode) {
        return new Result<T>(errorCode.getCode(), errorCode.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> Result<T> failed(String message) {
        return new Result<T>(ResultCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> Result<T> failed() {
        return failed(ResultCode.FAILED);
    }

    /**
     * 参数验证失败返回结果
     */
    public static <T> Result<T> validateFailed() {
        return failed(ResultCode.VALIDATE_FAILED);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> Result<T> validateFailed(String message) {
        return new Result<T>(ResultCode.VALIDATE_FAILED.getCode(), message, null);
    }

    /**
     * 未登录返回结果
     */
    public static <T> Result<T> unauthorized(T data) {
        return new Result<T>(ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getMessage(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> Result<T> forbidden(T data) {
        return new Result<T>(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMessage(), data);
    }

    public static <T> Result<T> handleResult(int i) {
        return new Result<T>(i > 0 ? ResultCode.SUCCESS.getCode() : ResultCode.FAILED.getCode(), i > 0 ? "处理成功" :
            "处理失败",
            null);
    }
}

