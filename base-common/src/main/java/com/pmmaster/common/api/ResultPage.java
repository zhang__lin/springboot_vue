package com.pmmaster.common.api;

import com.github.pagehelper.PageInfo;
import com.pmmaster.common.enums.ResultCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @Author: Always_
 * @Date: 2020/2/11
 */
@Getter
@Setter
public class ResultPage<T> {
    private long code;
    private long status;
    private String msg;
    private boolean success;
    private T data;
    private Integer page;
    private Integer pageSize;
    private Long total;
    private Integer totalPage;

    /**
     * 将PageHelper分页后的list转为分页信息
     */
    public static <T> ResultPage<T> restPage(List<T> list) {
        ResultPage<T> result = new ResultPage<T>();
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        result.setTotalPage(pageInfo.getPages());
        result.setPage(pageInfo.getPageNum());
        result.setPageSize(pageInfo.getPageSize());
        result.setTotal(pageInfo.getTotal());
        result.setData((T) pageInfo.getList());
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setStatus(result.getCode());
        result.setSuccess(true);
        result.setMsg("成功");
        return result;
    }

    /**
     * 将SpringData分页后的list转为分页信息
     */
    public static <T> ResultPage<T> restPage(Page<T> pageInfo) {
        ResultPage<T> result = new ResultPage<T>();
        result.setTotalPage(pageInfo.getTotalPages());
        result.setPage(pageInfo.getNumber());
        result.setPageSize(pageInfo.getSize());
        result.setTotal(pageInfo.getTotalElements());
        result.setData((T) pageInfo.getContent());
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setSuccess(true);
        result.setMsg("成功");
        return result;
    }
}
