package com.pmmaster.common.map;

import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传类型，大小
 *
 * @Author: Always_
 * @Date: 2020/2/10
 */
public class UploadType {
    public static Map<String, Long> map = new HashMap<String, Long>();

    static {
        //图片类型
        map.put("jpeg", new Long(5 * 1024 * 1024));//jpeg类型
        map.put("jpg", new Long(5 * 1024 * 1024));//jpg类型
        map.put("JPG", new Long(5 * 1024 * 1024));//JPG类型
        map.put("png", new Long(5 * 1024 * 1024));//png类型
        map.put("PNG", new Long(5 * 1024 * 1024));//png类型
        map.put("gif", new Long(5 * 1024 * 1024));//gif类型

        //文档类型
        map.put("vnd.ms-excel", new Long(5 * 1024 * 1024));//xls
        map.put("vnd.openxmlformats-officedocument.spreadsheetml.sheet", new Long(5 * 1024 * 1024));//xlsx
        map.put("msword", new Long(5 * 1024 * 1024));//doc
        map.put("vnd.openxmlformats-officedocument.wordprocessingml.document", new Long(5 * 1024 * 1024));//docx
        map.put("plain", new Long(5 * 1024 * 1024));//txt

        map.put("octet-stream", new Long(70 * 1024 * 1024));//octet-stream 基础流
        //视频
        map.put("mp4", new Long(70 * 1024 * 1024));//mp4
        map.put("avi", new Long(70 * 1024 * 1024));//.avi
        map.put("msvideo", new Long(70 * 1024 * 1024));//avi
        map.put("x-msvideo", new Long(70 * 1024 * 1024));//avi
        map.put("avs-video", new Long(70 * 1024 * 1024));//avi
        map.put("quicktime", new Long(70 * 1024 * 1024));//quicktime .mov

    }
}
