package com.pmmaster.admin.interceptor;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.enums.ResultCode;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class NotFoundException implements ErrorController {
    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping(value = {"/error"})
    @ResponseBody
    public Result error(Exception e) {
        return Result.failed(ResultCode.VALIDATE_FAILED);
    }
}
