package com.pmmaster.admin.controllers;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.api.ResultPage;
import com.pmmaster.common.entitis.Admin;
import com.pmmaster.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping("list")
    public ResultPage list(@RequestBody Admin admin) {
        return ResultPage.restPage(adminService.getList(admin));
    }

    @RequestMapping(value = "save")
    public Result save(HttpServletRequest request, @Validated @RequestBody Admin admin) {
        return adminService.save(request, admin);
    }

    @RequestMapping("info/{id}")
    public Result info(@PathVariable Integer id) {
        return adminService.info(id);
    }

    /**
     * 管理员授权
     *
     * @return
     */
    @RequestMapping("auth")
    public Result auth(@RequestBody Admin admin) {
        return adminService.auth(admin);
    }

    /**
     * 强制重置密码
     *
     * @return
     */
    @RequestMapping("resetPass")
    public Result resetPass(@RequestBody Admin admin) {
        return adminService.updatePass(admin);
    }

    @RequestMapping("delete")
    public Result delete(@RequestBody Admin admin) {
        return adminService.delete(admin);
    }
}
