package com.pmmaster.admin.controllers;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminMenu;
import com.pmmaster.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @Author: Always_
 * @Date: 2020/2/5
 */
@RestController
@RequestMapping("/menus")
public class MenusController {

    @Autowired
    private MenuService menuService;

    @RequestMapping("list")
    public Result menuList() {
        return Result.success(menuService.getList());
    }

    @RequestMapping("add")
    public Result add(@Validated @RequestBody AdminMenu menu) {
        return menuService.add(menu);
    }

    @RequestMapping("info/{id}")
    public Result info(@PathVariable Integer id) {
        return menuService.info(id);
    }

    @RequestMapping("update")
    public Result update(@Validated @RequestBody AdminMenu menu) {
        return menuService.update(menu);
    }

    @RequestMapping("delete")
    public Result delete(@RequestBody LinkedHashMap ids) {
        ArrayList<String> list = (ArrayList<String>) ids.get("ids");
        return menuService.delete(list);
    }

    @RequestMapping("restore")
    public Result restore(@RequestBody LinkedHashMap ids) {
        ArrayList<String> list = (ArrayList<String>) ids.get("ids");
        return menuService.restore(list);
    }
}
