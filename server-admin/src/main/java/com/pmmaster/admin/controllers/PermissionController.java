package com.pmmaster.admin.controllers;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminPermission;
import com.pmmaster.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @Author: Always_
 * @Date: 2020/2/5
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @RequestMapping("list")
    public Result list() {
        return Result.success(permissionService.getList());
    }

    @RequestMapping("add")
    public Result add(@Validated @RequestBody AdminPermission menu) {
        return permissionService.add(menu);
    }

    @RequestMapping("info/{id}")
    public Result info(@PathVariable Integer id) {
        return permissionService.info(id);
    }

    @RequestMapping("update")
    public Result update(@Validated @RequestBody AdminPermission menu) {
        return permissionService.update(menu);
    }

    @RequestMapping("delete")
    public Result delete(@RequestBody LinkedHashMap ids) {
        ArrayList<String> list = (ArrayList<String>) ids.get("ids");
        return permissionService.delete(list);
    }

    @RequestMapping("restore")
    public Result restore(@RequestBody LinkedHashMap ids) {
        ArrayList<String> list = (ArrayList<String>) ids.get("ids");
        return permissionService.restore(list);
    }
}
