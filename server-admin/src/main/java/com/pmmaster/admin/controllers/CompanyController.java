package com.pmmaster.admin.controllers;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.api.ResultPage;
import com.pmmaster.common.entitis.Company;
import com.pmmaster.common.entitis.CompanyDepartment;
import com.pmmaster.common.enums.CompanyRole;
import com.pmmaster.common.enums.CompanyType;
import com.pmmaster.common.enums.SupplierType;
import com.pmmaster.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Always_
 * @Date: 2020/2/10
 */
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @RequestMapping("list")
    public ResultPage list(@RequestBody Company company) {
        return ResultPage.restPage(companyService.getList(company));
    }

    @RequestMapping("config")
    public Result config() {
        Map<String, Object> map = new HashMap<>();
        map.put("company_roles", CompanyRole.list());
        map.put("supplier_types", SupplierType.list());
        map.put("company_types", CompanyType.list());
        return Result.success(map);
    }

    @RequestMapping("info/{id}")
    public Result info(@PathVariable Integer id) {
        return companyService.info(id);
    }

    @RequestMapping("save")
    public Result save(@Validated @RequestBody Company company) {
        return companyService.save(company);
    }

    @RequestMapping("departments/{id}")
    public Result departments(HttpServletRequest request, @PathVariable Integer id) {
        return Result.success(companyService.getDepartmentList(request, id));
    }

    @RequestMapping("saveDepartment")
    public Result saveDepartment(@Validated @RequestBody CompanyDepartment department) {
        return companyService.saveDepartment(department);
    }

    @RequestMapping("departmentInfo/{id}")
    public Result departmentInfo(@PathVariable Integer id) {
        return companyService.departmentInfo(id);
    }

    @RequestMapping("deleteDepartment")
    public Result deleteDepartment(@RequestBody CompanyDepartment department) {
        return companyService.deleteDepartment(department);
    }
}
