package com.pmmaster.admin.controllers;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.api.ResultPage;
import com.pmmaster.common.entitis.AdminRole;
import com.pmmaster.common.enums.CommonState;
import com.pmmaster.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色控制器
 *
 * @Author: Always_
 * @Date: 2020/2/8
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 角色列表
     *
     * @return
     */
    @RequestMapping("list")
    public ResultPage list(@RequestBody AdminRole role) {
        return ResultPage.restPage(roleService.getList(role));
    }

    /**
     * 角色添加
     *
     * @param role
     * @return
     */
    @RequestMapping("add")
    public Result add(@Validated @RequestBody AdminRole role) {
        return roleService.add(role);
    }

    /**
     * 指定角色信息
     *
     * @return
     */
    @RequestMapping("/info/{id}")
    public Result info(@PathVariable Integer id) {
        return roleService.info(id);
    }

    /**
     * 更新角色信息
     *
     * @param role
     * @return
     */
    @RequestMapping("update")
    public Result update(@Validated @RequestBody AdminRole role) {
        return roleService.update(role);
    }

    /**
     * 角色禁用
     *
     * @param id
     * @return
     */
    @RequestMapping("delete/{id}")
    public Result delete(@PathVariable Integer id) {
        return roleService.updateState(id, CommonState.DELETED.getState());
    }

    /**
     * 角色恢复
     *
     * @param id
     * @return
     */
    @RequestMapping("restore/{id}")
    public Result restore(@PathVariable Integer id) {
        return roleService.updateState(id, CommonState.NORMAL.getState());
    }
}
