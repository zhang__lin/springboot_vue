package com.pmmaster.admin.controllers;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.Admin;
import com.pmmaster.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;

@RestController
@RequestMapping("/")
public class AuthController {

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${super_users}")
    private Integer[] super_users;

    @Autowired
    private AdminService adminService;

    @RequestMapping("login")
    public Result login(@RequestBody Admin admin) {
        return adminService.login(admin, tokenHead);
    }

    @RequestMapping("loginUser")
    public Result loginUser(HttpServletRequest request) {
        return Result.success(request.getSession().getAttribute("loginUser"));
    }

    @RequestMapping(value = "/freshRole", method = RequestMethod.POST)
    public Result roleInfo(HttpServletRequest request){
        Admin user = (Admin) request.getSession().getAttribute("loginUser");
        Map<String,Object> map = null;
        if(user != null){
            map = adminService.userPermissionInfo(user, Arrays.asList(super_users));
        }
        return Result.success(map);
    }
}
