/*
Navicat MySQL Data Transfer

Source Server         : always
Source Server Version : 80012
Source Host           : 120.78.182.218:3306
Source Database       : xfw

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2020-02-13 17:37:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xfw_admin_menus
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admin_menus`;
CREATE TABLE `xfw_admin_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '菜单名称',
  `value` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '模版地址',
  `icon` varchar(255) DEFAULT NULL COMMENT 'icon',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否隐藏1是0否',
  `parent_id` int(11) NOT NULL,
  `order` int(11) DEFAULT '1' COMMENT '排序',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单';

-- ----------------------------
-- Records of xfw_admin_menus
-- ----------------------------
INSERT INTO `xfw_admin_menus` VALUES ('1', '顶级菜单', null, null, null, '0', '0', '1', '2020-02-05 17:02:57', '2020-02-05 17:02:59', null);
INSERT INTO `xfw_admin_menus` VALUES ('2', '系统设置', null, null, null, '0', '1', '1', '2020-02-05 17:58:44', '2020-02-05 17:58:44', null);
INSERT INTO `xfw_admin_menus` VALUES ('3', '菜单管理', 'system/menu', 'system/menu', null, '0', '2', '2', '2020-02-05 18:01:38', '2020-02-05 18:01:38', null);
INSERT INTO `xfw_admin_menus` VALUES ('4', '权限管理', 'system/permission', 'system/permission', null, '0', '2', '2', '2020-02-05 18:02:42', '2020-02-11 16:36:38', null);
INSERT INTO `xfw_admin_menus` VALUES ('5', '角色管理', 'system/role', 'system/role', null, '1', '2', '2', '2020-02-05 18:02:52', '2020-02-11 16:36:54', null);
INSERT INTO `xfw_admin_menus` VALUES ('6', '管理员管理', 'system/admin', 'system/admin', null, '0', '2', '3', '2020-02-11 16:37:12', '2020-02-11 16:37:12', null);
INSERT INTO `xfw_admin_menus` VALUES ('7', '机构管理', null, null, null, '0', '1', '2', '2020-02-11 16:37:28', '2020-02-11 16:37:28', null);
INSERT INTO `xfw_admin_menus` VALUES ('8', '机构列表', 'company/index', 'company/index', null, '0', '7', '1', '2020-02-11 16:37:45', '2020-02-11 16:37:45', null);
INSERT INTO `xfw_admin_menus` VALUES ('9', '机构添加', 'company/add', 'company/add', null, '0', '7', '2', '2020-02-11 16:38:03', '2020-02-11 16:38:03', null);

-- ----------------------------
-- Table structure for xfw_admin_permissions
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admin_permissions`;
CREATE TABLE `xfw_admin_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `alias` varchar(100) DEFAULT NULL COMMENT '操作权限名称',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限地址',
  `parent_id` int(11) DEFAULT '0' COMMENT '父权限',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xfw_admin_permissions
-- ----------------------------
INSERT INTO `xfw_admin_permissions` VALUES ('1', '顶级权限', null, null, '0', '2020-02-08 14:30:35', '2020-02-08 14:30:37', null);
INSERT INTO `xfw_admin_permissions` VALUES ('2', '公共权限', null, null, '1', '2020-02-13 16:18:22', '2020-02-13 16:18:22', null);
INSERT INTO `xfw_admin_permissions` VALUES ('3', '账户登录', '/login', '/login', '2', '2020-02-13 16:18:52', '2020-02-13 16:18:52', null);
INSERT INTO `xfw_admin_permissions` VALUES ('4', '文件上传', '/upload/{type}', '/upload/{type}', '2', '2020-02-13 16:19:37', '2020-02-13 16:19:37', null);
INSERT INTO `xfw_admin_permissions` VALUES ('5', '个人信息', '/loginUser', '/loginUser', '2', '2020-02-13 16:24:47', '2020-02-13 16:24:47', null);
INSERT INTO `xfw_admin_permissions` VALUES ('6', '我的权限', '/freshRole', '/freshRole', '2', '2020-02-13 16:24:59', '2020-02-13 16:24:59', null);

-- ----------------------------
-- Table structure for xfw_admin_role_infos
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admin_role_infos`;
CREATE TABLE `xfw_admin_role_infos` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xfw_admin_role_infos
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_admin_role_menus
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admin_role_menus`;
CREATE TABLE `xfw_admin_role_menus` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单';

-- ----------------------------
-- Records of xfw_admin_role_menus
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_admin_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admin_role_permissions`;
CREATE TABLE `xfw_admin_role_permissions` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `permission_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xfw_admin_role_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admin_roles`;
CREATE TABLE `xfw_admin_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态0删除1正常',
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xfw_admin_roles
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_admins
-- ----------------------------
DROP TABLE IF EXISTS `xfw_admins`;
CREATE TABLE `xfw_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL COMMENT '部门编号',
  `name` varchar(80) NOT NULL COMMENT '账号',
  `nick_name` varchar(20) DEFAULT NULL COMMENT '昵称',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `password` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '真实姓名',
  `id_number` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '身份证号',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '邮箱',
  `open_id` varchar(60) DEFAULT NULL,
  `avatar` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '头像',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态-1已删除0禁用1正常',
  `last_login_at` datetime DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '上次登录IP',
  `now_login_at` datetime DEFAULT NULL COMMENT '本次登录时间',
  `now_login_ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '本次登陆ip',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='管理员';

-- ----------------------------
-- Records of xfw_admins
-- ----------------------------
INSERT INTO `xfw_admins` VALUES ('1', null, 'admin', null, '18613236517', '$2y$10$Npit2TQrdZ3l3Jy4YJxOQOnMzFYs8DV6VdYr9xwfy9zw1XCb4wrYu', null, null, null, null, '', '1', '2020-02-01 19:10:43', '127.0.0.1', '2020-02-01 19:10:43', '127.0.0.1', '2020-02-01 19:10:43', '2020-02-11 16:24:16');

-- ----------------------------
-- Table structure for xfw_companies
-- ----------------------------
DROP TABLE IF EXISTS `xfw_companies`;
CREATE TABLE `xfw_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinyint(2) NOT NULL DEFAULT '1' COMMENT '公司角色CompanyRole Enum',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构名称',
  `short_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '机构简称',
  `rate` decimal(2,1) DEFAULT '3.0' COMMENT '客户等级1-5级',
  `company_type` tinyint(2) DEFAULT NULL COMMENT '机构类别',
  `state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态：1正常0禁用',
  `ticket_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '开票名称',
  `register_no` varchar(25) DEFAULT NULL COMMENT '税号',
  `register_address` varchar(255) DEFAULT NULL COMMENT '注册场所地址',
  `register_phone` varchar(20) DEFAULT NULL COMMENT '注册电话',
  `bank_name` varchar(200) DEFAULT NULL COMMENT '开户行名称',
  `bank_no` varchar(50) DEFAULT NULL COMMENT '开户行账号',
  `regions` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所在省市县',
  `address` varchar(500) DEFAULT NULL COMMENT '详细地址',
  `mobile` varchar(30) DEFAULT NULL COMMENT '固定电话',
  `url` varchar(200) DEFAULT NULL COMMENT '官网地址',
  `supplier_type` tinyint(2) DEFAULT NULL COMMENT '供应商类型',
  `contact_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人姓名',
  `contact_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人手机号',
  `licence_pic` varchar(200) DEFAULT NULL COMMENT '营业执照',
  `desc` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '机构备注',
  `build_at` date DEFAULT NULL COMMENT '成立时间',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  UNIQUE KEY `ticket_name` (`ticket_name`),
  UNIQUE KEY `register_no` (`register_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构';

-- ----------------------------
-- Records of xfw_companies
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_company_applies
-- ----------------------------
DROP TABLE IF EXISTS `xfw_company_applies`;
CREATE TABLE `xfw_company_applies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '申请用户ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '机构名称',
  `short_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '简称',
  `department_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所在部门',
  `company_type` int(11) DEFAULT NULL COMMENT '机构类型',
  `regions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所在地',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '详细地',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '发票类型',
  `header_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '发票抬头',
  `register_no` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '税号',
  `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '开户行名称',
  `bank_no` varchar(30) DEFAULT NULL COMMENT '开户行账户',
  `licence_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '营业执照',
  `state` tinyint(4) DEFAULT '0' COMMENT '状态0待审核1审核通过2审核驳回',
  `desc` varchar(3500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'b备注',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='机构账户升级申请表';

-- ----------------------------
-- Records of xfw_company_applies
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_company_departments
-- ----------------------------
DROP TABLE IF EXISTS `xfw_company_departments`;
CREATE TABLE `xfw_company_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL COMMENT '机构ID',
  `name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态1正常0禁用',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构部门关联';

-- ----------------------------
-- Records of xfw_company_departments
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_company_invoices
-- ----------------------------
DROP TABLE IF EXISTS `xfw_company_invoices`;
CREATE TABLE `xfw_company_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL COMMENT '公司ID',
  `header_title` varchar(255) NOT NULL COMMENT '发票抬头',
  `type` tinyint(4) DEFAULT NULL COMMENT '发票类型0增值税普通发票1企业增值税普通发票2增值税专用发票3组织（非企业）增值税普通发票',
  `register_no` varchar(25) DEFAULT NULL COMMENT '税务登记证号',
  `bank_name` varchar(255) DEFAULT NULL COMMENT '开户行名称',
  `bank_no` varchar(100) DEFAULT NULL COMMENT '基本开户账号',
  `register_address` varchar(255) DEFAULT NULL COMMENT '注册场所地址',
  `register_phone` varchar(20) DEFAULT NULL COMMENT '注册固定电话',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`) USING BTREE,
  UNIQUE KEY `register_no` (`register_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构开票信息';

-- ----------------------------
-- Records of xfw_company_invoices
-- ----------------------------

-- ----------------------------
-- Table structure for xfw_files
-- ----------------------------
DROP TABLE IF EXISTS `xfw_files`;
CREATE TABLE `xfw_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL COMMENT '上传位置',
  `originalName` varchar(230) NOT NULL COMMENT '上传的文件名',
  `mimeType` varchar(50) NOT NULL COMMENT '文件类型',
  `file_path` varchar(150) NOT NULL COMMENT '所在路径',
  `size` double NOT NULL COMMENT '文件大小',
  `width_height` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片尺寸',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_path` (`file_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片库';

-- ----------------------------
-- Records of xfw_files
-- ----------------------------
