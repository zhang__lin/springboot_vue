# springboot_vue

#### 介绍
springboot+jwt+elementui搭建，已完成权限分配、鉴权模块

#### 软件架构
springboot+MYSQL+REDIS+elementui


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xfw.sql为MYSQL数据库文件，里面有详细表说明以及字段说明
2.  maven依赖自动引入
3.  web-admin为管理后台页面，进入web-admin目录，执行npm install命令安装nodejs依赖，npm run watch进入开发模式，npm run prod打包正式环境文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
