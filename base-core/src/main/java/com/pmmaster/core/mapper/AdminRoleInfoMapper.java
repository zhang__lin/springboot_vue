package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.AdminRoleInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminRoleInfoMapper {
    int insert(AdminRoleInfo record);

    int insertSelective(AdminRoleInfo record);

    int deleteByUser(Integer userId);

    int deleteByRole(Integer roleId);

    List<Integer> queryUserRole(Integer userId);

    /**
     * 用户角色信息
     *
     * @param id
     * @return
     */
    List<AdminRoleInfo> getRoleInfo(Integer id);
}
