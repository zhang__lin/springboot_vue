package com.pmmaster.core.mapper;

import com.github.pagehelper.Page;
import com.pmmaster.common.entitis.CompanyDepartment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CompanyDepartmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CompanyDepartment record);

    int insertSelective(CompanyDepartment record);

    CompanyDepartment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CompanyDepartment record);

    int updateByPrimaryKey(CompanyDepartment record);

    Page<CompanyDepartment> list(CompanyDepartment department);
}
