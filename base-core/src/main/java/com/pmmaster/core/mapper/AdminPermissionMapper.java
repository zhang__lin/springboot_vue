package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.AdminPermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminPermissionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminPermission record);

    int insertSelective(AdminPermission record);

    AdminPermission selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminPermission record);

    int updateByPrimaryKey(AdminPermission record);

    List<AdminPermission> getList();

    /**
     * 指定用户权限列表
     *
     * @param userId
     * @return
     */
    List<AdminPermission> getUserPermissionList(Integer userId);
}
