package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.CompanyApply;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CompanyApplyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CompanyApply record);

    int insertSelective(CompanyApply record);

    CompanyApply selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CompanyApply record);

    int updateByPrimaryKey(CompanyApply record);
}