package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.AdminRoleMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface AdminRoleMenuMapper {
    int insert(AdminRoleMenu record);

    int insertSelective(AdminRoleMenu record);

    int batchInsert(List<AdminRoleMenu> list);

    ArrayList<Integer> selectByRole(Integer roleId);

    void deleteByRole(Integer roleId);
}
