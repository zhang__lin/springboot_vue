package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.CompanyInvoice;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CompanyInvoiceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CompanyInvoice record);

    int insertSelective(CompanyInvoice record);

    CompanyInvoice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CompanyInvoice record);

    int updateByPrimaryKey(CompanyInvoice record);
}