package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.AdminRolePermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface AdminRolePermissionMapper {
    int insert(AdminRolePermission record);

    int insertSelective(AdminRolePermission record);

    int batchInsert(List<AdminRolePermission> list);

    ArrayList<Integer> selectByRole(Integer roleId);

    void deleteByRole(Integer roleId);
}
