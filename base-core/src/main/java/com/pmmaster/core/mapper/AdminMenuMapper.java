package com.pmmaster.core.mapper;

import com.pmmaster.common.entitis.AdminMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminMenu record);

    int insertSelective(AdminMenu record);

    AdminMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminMenu record);

    int updateByPrimaryKey(AdminMenu record);

    List<AdminMenu> getList();

    /**
     * 指定用户的权限菜单
     *
     * @param userId
     * @return
     */
    List<AdminMenu> getUserMenuList(Integer userId);
}
