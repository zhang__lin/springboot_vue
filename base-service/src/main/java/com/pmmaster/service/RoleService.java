package com.pmmaster.service;

import com.github.pagehelper.Page;
import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminRole;

/**
 * @Author: Always_
 * @Date: 2020/2/8
 */
public interface RoleService {
    Page<AdminRole> getList(AdminRole role);

    Result add(AdminRole role);

    Result info(Integer id);

    Result update(AdminRole role);

    Result updateState(Integer id, Integer state);
}
