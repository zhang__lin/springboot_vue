package com.pmmaster.service;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminPermission;

import java.util.ArrayList;

/**
 * @Author: Always_
 * @Date: 2020/2/5
 */
public interface PermissionService {
    AdminPermission getList();

    Result add(AdminPermission menu);

    Result info(Integer id);

    Result update(AdminPermission menu);

    Result delete(ArrayList<String> list);

    Result restore(ArrayList<String> list);
}
