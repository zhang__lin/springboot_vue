package com.pmmaster.service;

import com.github.pagehelper.Page;
import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.Admin;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface AdminService {

    /**
     * 获取登录用户
     *
     * @param name
     * @return
     */
    Admin getLoginUser(HttpServletRequest request, String tokenHead, String tokenHeader);

    /**
     * 保存用户信息
     *
     * @param admin
     * @return
     */
    Result save(HttpServletRequest request, Admin admin);

    /**
     * 账户登录
     *
     * @param admin
     * @return
     */
    Result login(Admin admin, String tokenHead);

    /**
     * 用户权限，菜单信息
     *
     * @param user
     * @param asList
     * @return
     */
    Map<String, Object> userPermissionInfo(Admin user, List<Integer> asList);

    Page<Admin> getList(Admin admin);

    Result info(Integer id);

    Result auth(Admin admin);

    Result updatePass(Admin admin);

    Result delete(Admin admin);

    /**
     * 判断用户是否有权限
     *
     * @param request
     * @param super_users
     * @return
     */
    boolean checkPermission(HttpServletRequest request, Integer[] super_users);
}
