package com.pmmaster.service;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminMenu;

import java.util.ArrayList;

public interface MenuService {
    AdminMenu getList();

    Result add(AdminMenu menu);

    Result info(Integer id);

    Result update(AdminMenu menu);

    Result delete(ArrayList<String> list);

    Result restore(ArrayList<String> list);

}
