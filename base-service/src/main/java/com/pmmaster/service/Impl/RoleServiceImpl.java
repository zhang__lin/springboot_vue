package com.pmmaster.service.Impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminRole;
import com.pmmaster.common.entitis.AdminRoleMenu;
import com.pmmaster.common.entitis.AdminRolePermission;
import com.pmmaster.common.enums.CommonState;
import com.pmmaster.core.mapper.AdminRoleMapper;
import com.pmmaster.core.mapper.AdminRoleMenuMapper;
import com.pmmaster.core.mapper.AdminRolePermissionMapper;
import com.pmmaster.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: Always_
 * @Date: 2020/2/8
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private AdminRoleMapper roleMapper;
    @Autowired
    private AdminRolePermissionMapper rolePermissionMapper;
    @Autowired
    private AdminRoleMenuMapper roleMenuMapper;

    @Override
    public Page<AdminRole> getList(AdminRole role) {
        PageHelper.startPage(role.getPage(), role.getPageSize());
        return roleMapper.list(role);
    }

    @Override
    public Result info(Integer id) {
        AdminRole role = roleMapper.selectByPrimaryKey(id);
        if (role == null) {
            return Result.failed("角色不存在");
        }
        role.setPermissions(rolePermissionMapper.selectByRole(role.getId()));
        role.setMenus(roleMenuMapper.selectByRole(role.getId()));
        return Result.success(role);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Result add(AdminRole role) {
        role.setCreatedAt(new Date());
        role.setUpdatedAt(role.getCreatedAt());
        role.setState(CommonState.NORMAL.getState());
        roleMapper.insertSelective(role);

        List<AdminRolePermission> rolePermissionList = handleToRolePermission(role);
        List<AdminRoleMenu> roleMenuList = handleToRoleMenu(role);

        if (!CollectionUtils.isEmpty(rolePermissionList)) {
            rolePermissionMapper.batchInsert(rolePermissionList);
        }
        if (!CollectionUtils.isEmpty(roleMenuList)) {
            roleMenuMapper.batchInsert(roleMenuList);
        }
        return Result.success("添加成功");
    }

    /**
     * 处理角色权限
     *
     * @param role
     * @return
     */
    private List<AdminRolePermission> handleToRolePermission(AdminRole role) {
        List<AdminRolePermission> rolePermissionList = new ArrayList<>();
        for (Integer pid : role.getPermissions()) {
            rolePermissionList.add(new AdminRolePermission(role.getId(), pid));
        }
        return rolePermissionList;
    }

    /**
     * 处理角色菜单
     *
     * @param role
     * @return
     */
    private List<AdminRoleMenu> handleToRoleMenu(AdminRole role) {
        List<AdminRoleMenu> roleMenuList = new ArrayList<>();
        for (Integer mid : role.getMenus()) {
            roleMenuList.add(new AdminRoleMenu(role.getId(), mid));
        }
        return roleMenuList;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public Result update(AdminRole role) {
        role.setUpdatedAt(new Date());
        if (roleMapper.updateByPrimaryKeySelective(role) > 0) {
            rolePermissionMapper.deleteByRole(role.getId());
            roleMenuMapper.deleteByRole(role.getId());

            List<AdminRolePermission> rolePermissionList = handleToRolePermission(role);
            List<AdminRoleMenu> roleMenuList = handleToRoleMenu(role);

            if (!CollectionUtils.isEmpty(rolePermissionList)) {
                rolePermissionMapper.batchInsert(rolePermissionList);
            }
            if (!CollectionUtils.isEmpty(roleMenuList)) {
                roleMenuMapper.batchInsert(roleMenuList);
            }
            return Result.success("更新成功");
        }
        return Result.failed("角色基本信息更新失败");
    }

    @Override
    public Result updateState(Integer id, Integer state) {
        AdminRole role = new AdminRole(id, state, new Date());
        return Result.optionResult(roleMapper.updateByPrimaryKeySelective(role));
    }
}
