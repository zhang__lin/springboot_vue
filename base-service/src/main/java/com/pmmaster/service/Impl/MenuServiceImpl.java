package com.pmmaster.service.Impl;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminMenu;
import com.pmmaster.common.utils.TreeUtil;
import com.pmmaster.core.mapper.AdminMenuMapper;
import com.pmmaster.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private AdminMenuMapper menuMapper;

    @Override
    public AdminMenu getList() {
        List<AdminMenu> list = menuMapper.getList();
        return TreeUtil.handleMenuTree(list);
    }

    @Override
    public Result add(AdminMenu menu) {
        menu.setCreatedAt(new Date());
        menu.setUpdatedAt(menu.getCreatedAt());

        return Result.addResult(menuMapper.insert(menu));
    }

    @Override
    public Result info(Integer id) {
        return Result.success(menuMapper.selectByPrimaryKey(id));
    }

    @Override
    public Result update(AdminMenu menu) {
        menu.setUpdatedAt(new Date());
        return Result.updateResult(menuMapper.updateByPrimaryKey(menu));
    }

    @Override
    public Result delete(ArrayList<String> ids) {
        for (String id : ids) {
            AdminMenu menu = new AdminMenu();
            menu.setId(Integer.valueOf(id));
            menu.setUpdatedAt(new Date());
            menu.setDeletedAt(menu.getUpdatedAt());
            menuMapper.updateByPrimaryKeySelective(menu);
        }
        return Result.success(null, "处理成功");
    }

    @Override
    public Result restore(ArrayList<String> ids) {
        for (String id : ids) {
            AdminMenu menu = new AdminMenu();
            menu.setId(Integer.valueOf(id));
            menu.setUpdatedAt(new Date());
            menu.setDeletedAt(null);
            menuMapper.updateByPrimaryKeySelective(menu);
        }
        return Result.success(null, "处理成功");
    }
}
