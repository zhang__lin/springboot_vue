package com.pmmaster.service.Impl;

import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.AdminPermission;
import com.pmmaster.common.utils.TreeUtil;
import com.pmmaster.core.mapper.AdminPermissionMapper;
import com.pmmaster.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: Always_
 * @Date: 2020/2/5
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private AdminPermissionMapper permissionMapper;

    @Override
    public AdminPermission getList() {
        List<AdminPermission> list = permissionMapper.getList();
        return TreeUtil.handlePermissionTree(list);
    }

    @Override
    public Result add(AdminPermission permission) {
        permission.setCreatedAt(new Date());
        permission.setUpdatedAt(permission.getCreatedAt());
        permission.setParentId(permission.getParentId() == null ? 0 : permission.getParentId());

        return Result.addResult(permissionMapper.insert(permission));
    }

    @Override
    public Result info(Integer id) {
        return Result.success(permissionMapper.selectByPrimaryKey(id));
    }

    @Override
    public Result update(AdminPermission permission) {
        permission.setUpdatedAt(new Date());
        return Result.updateResult(permissionMapper.updateByPrimaryKey(permission));
    }

    @Override
    public Result delete(ArrayList<String> list) {
        for (String id : list) {
            AdminPermission permission = new AdminPermission();
            permission.setId(Integer.valueOf(id));
            permission.setUpdatedAt(new Date());
            permission.setDeletedAt(permission.getUpdatedAt());
            permissionMapper.updateByPrimaryKeySelective(permission);
        }
        return Result.success(null, "处理成功");
    }

    @Override
    public Result restore(ArrayList<String> list) {
        for (String id : list) {
            AdminPermission permission = new AdminPermission();
            permission.setId(Integer.valueOf(id));
            permission.setUpdatedAt(new Date());
            permission.setDeletedAt(null);
            permissionMapper.updateByPrimaryKeySelective(permission);
        }
        return Result.success(null, "处理成功");
    }
}
