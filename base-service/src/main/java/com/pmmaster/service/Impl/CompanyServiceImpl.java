package com.pmmaster.service.Impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.Company;
import com.pmmaster.common.entitis.CompanyDepartment;
import com.pmmaster.common.enums.CommonState;
import com.pmmaster.common.enums.CompanyRole;
import com.pmmaster.common.utils.StringUtil;
import com.pmmaster.core.mapper.CompanyDepartmentMapper;
import com.pmmaster.core.mapper.CompanyMapper;
import com.pmmaster.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @Author: Always_
 * @Date: 2020/2/10
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private CompanyDepartmentMapper departmentMapper;

    private String error;

    @Override
    public List<Company> getList(Company company) {
        PageHelper.startPage(company.getPage(), company.getPageSize());
        return companyMapper.list(company);
    }

    @Override
    public Result info(Integer id) {
        Company company = companyMapper.selectByPrimaryKey(id);
        company.setRegionsArr(StringUtil.explode(company.getRegions(), " "));
        return company == null ? Result.failed("机构不存在") : Result.success(company);
    }

    @Override
    public Page<CompanyDepartment> getDepartmentList(HttpServletRequest request, Integer id) {
        CompanyDepartment department = new CompanyDepartment();
        department.setCompanyId(id);
        department.setName(request.getParameter("keyword"));
        return departmentMapper.list(department);
    }

    @Override
    public Result saveDepartment(CompanyDepartment department) {
        if (department.getId() == null) {
            //新增
            department.setCreatedAt(new Date());
            department.setState(CommonState.NORMAL.getState());
            department.setUpdatedAt(department.getCreatedAt());
            return Result.addResult(departmentMapper.insertSelective(department));
        } else {
            //修改
            department.setUpdatedAt(new Date());
            return Result.updateResult(departmentMapper.updateByPrimaryKeySelective(department));
        }
    }

    @Override
    public Result departmentInfo(Integer id) {
        CompanyDepartment department = departmentMapper.selectByPrimaryKey(id);
        return department == null ? Result.failed("部门不存") : Result.success(department);
    }

    @Override
    public Result deleteDepartment(CompanyDepartment department) {
        department.setUpdatedAt(new Date());
        return Result.optionResult(departmentMapper.updateByPrimaryKeySelective(department));
    }

    @Override
    public Result save(Company company) {
        company.setRegions(StringUtil.implode(company.getRegionsArr(), " "));
        company.setTicketName(company.getTicketName() == null ? company.getName() : company.getTicketName());
        company.setUrl(company.getUrlPrepend() + company.getUrl());
        if (company.getId() == null) {
            //新增
            if (!this.checkCanAdd(company)) {
                return Result.failed(this.error);
            }
            company.setState(1);
            company.setCreatedAt(new Date());
            company.setUpdatedAt(company.getCreatedAt());
            return Result.addResult(companyMapper.insertSelective(company));
        } else {
            company.setUpdatedAt(new Date());
            return Result.updateResult(companyMapper.updateByPrimaryKeySelective(company));
        }
    }

    /**
     * 添加前参数校验
     *
     * @param company
     * @return
     */
    private Boolean checkCanAdd(Company company) {
        Company com = new Company();
        if (company.getRole().equals(CompanyRole.SELF.getCode())) {
            //平台，检查是否已经存在了
            com.setRole(company.getRole());
            if (companyMapper.selectByExample(com) != null) {
                this.error = "只能添加一个平台机构";
                return false;
            }
        }
        com.setRole(null);
        com.setName(company.getName());
        if (companyMapper.selectByExample(com) != null) {
            this.error = "机构名称已经存在了";
            return false;
        }
        com.setName(null);
        com.setTicketName(company.getTicketName());
        if (companyMapper.selectByExample(com) != null) {
            this.error = "开票名称已被其他机构占用";
            return false;
        }
        com.setTicketName(null);
        com.setRegisterNo(company.getRegisterNo());
        if (companyMapper.selectByExample(com) != null) {
            this.error = "税号已被其他机构占用";
            return false;
        }
        return true;
    }
}
