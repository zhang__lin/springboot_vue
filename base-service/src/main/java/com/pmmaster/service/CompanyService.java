package com.pmmaster.service;

import com.github.pagehelper.Page;
import com.pmmaster.common.api.Result;
import com.pmmaster.common.entitis.Company;
import com.pmmaster.common.entitis.CompanyDepartment;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author: Always_
 * @Date: 2020/2/10
 */
public interface CompanyService {
    List<Company> getList(Company company);

    /**
     * 机构新增/修改
     *
     * @param company
     * @return
     */
    Result save(Company company);

    /**
     * 指定机构信息
     *
     * @param id
     * @return
     */
    Result info(Integer id);

    /**
     * 指定机构的部门列表
     *
     * @param request
     * @param id
     * @return
     */
    Page<CompanyDepartment> getDepartmentList(HttpServletRequest request, Integer id);

    /**
     * 保存部门
     *
     * @param department
     * @return
     */
    Result saveDepartment(CompanyDepartment department);

    /**
     * 部门信息
     *
     * @param id
     * @return
     */
    Result departmentInfo(Integer id);

    /**
     * 部门禁用
     *
     * @param department
     * @return
     */
    Result deleteDepartment(CompanyDepartment department);
}
