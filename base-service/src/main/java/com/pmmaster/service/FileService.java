package com.pmmaster.service;

import com.pmmaster.common.api.Result;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author: Always_
 * @Date: 2020/2/10
 */
public interface FileService {
    /**
     * 文件上传
     *
     * @param file     文件
     * @param basePath 保存基本路径
     * @return
     */
    Result upload(MultipartFile file, String pathPrefix, String basePath, String type);
}
