package com.pmmaster.common.entitis;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 图片库
 * xfw_files
 * @author Always_
 * @date 2020-02-10 18:11:28
 */
@Data
public class File implements Serializable {
    /**
     */
    private Integer id;

    /**
     * 上传位置
     */
    private String type;

    /**
     * 上传的文件名
     */
    private String originalname;

    /**
     * 文件类型
     */
    private String mimetype;

    /**
     * 所在路径
     */
    private String filePath;

    /**
     * 文件大小
     */
    private Double size;

    /**
     * 图片尺寸
     */
    private String widthHeight;

    /**
     */
    private Date createdAt;

    private static final long serialVersionUID = 1L;
}