package com.pmmaster.service;

import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;

public class SourcService {

    private static String entitySourcePath = "E:/workspace/xfw/utils-generator/src/main/java/com/pmmaster/common" +
        "/entitis";

    private static String entityTargetPath = "E:/workspace/xfw/base-common/src/main/java/com/pmmaster/common/entitis";

    private static String coreSourcePath = "E:/workspace/xfw/utils-generator/src/main/java/com/pmmaster/core";

    private static String coreTargetPath = "E:/workspace/xfw/base-core/src/main/java/com/pmmaster/core";

    public static void copySource() throws IOException {
        System.out.println("-----------开始拷贝实体类----------");
        copyEntites();
        System.out.println("-----------实体类拷贝完毕----------");

        System.out.println("-----------开始拷贝mapper----------");
        copyMappers();
        System.out.println("-----------mapper拷贝完毕----------");

        System.out.println("-----------开始拷贝mapping----------");
        copyMappings();
        System.out.println("-----------mapping拷贝完毕----------");
    }


    /**
     * 拷贝实体类
     */
    public static void copyEntites() throws IOException {
        File file = new File(entitySourcePath);
        File[] list = file.listFiles();
        for (File entity : list) {
            String nowName = entity.getName();
            if (!new File(entityTargetPath + "/" + nowName).exists()) {
                FileCopyUtils.copy(entity, new File(entityTargetPath + "/" + nowName));
                System.out.println(nowName + " Copy完成.....");
            } else {
                System.out.println(nowName + "已经存在了");
            }
        }
    }

    /**
     * mapper拷贝
     */
    public static void copyMappers() throws IOException {
        File file = new File(coreSourcePath + "/" + "mapper");
        File[] list = file.listFiles();
        for (File entity : list) {
            String nowName = entity.getName();
            if (!new File(coreTargetPath + "/mapper/" + nowName).exists()) {
                FileCopyUtils.copy(entity, new File(coreTargetPath + "/mapper/" + nowName));
                System.out.println(nowName + " Copy完成.....");
            } else {
                System.out.println(nowName + "已经存在了");
            }
        }
    }

    /**
     * mapping
     */
    public static void copyMappings() throws IOException {
        File file = new File(coreSourcePath + "/" + "mapping");
        File[] list = file.listFiles();
        for (File entity : list) {
            String nowName = entity.getName();
            if (!new File(coreTargetPath + "/mapping/" + nowName).exists()) {
                FileCopyUtils.copy(entity, new File(coreTargetPath + "/mapping/" + nowName));
                System.out.println(nowName + " Copy完成.....");
            } else {
                System.out.println(nowName + "已经存在了");
            }
        }
    }
}
