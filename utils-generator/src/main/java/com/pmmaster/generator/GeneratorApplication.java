package com.pmmaster.generator;

import com.pmmaster.service.SourcService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class GeneratorApplication {

    public static void main(String[] args) throws IOException {
        SourcService.copySource();
        SpringApplication.run(GeneratorApplication.class, args);
    }

}
